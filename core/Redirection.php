<?php
/**
 * Created by d0Nt
 * Date: 2018.05.15
 * Time: 14:39
 */

namespace core;


class Redirection
{
    public static function home(){
        header('Location: '.Helper::config("app")->directory.'/');
    }
    public static function appDir($route, $messages = []){
        Session::set("status_messages", $messages);
        header('Location: '.Helper::config("app")->directory.'/'.$route);
    }
}
<?php
/**
 * Created by d0Nt
 * Date: 2018.03.24
 * Time: 22:08
 */

namespace core;

use Twig_Environment;
use Twig_Loader_Filesystem;

class View
{
    private $data = [];
    public function setVar($varName, $value){
        $this->data[$varName] = $value;
    }

    function __set($name, $value){
        $this->setVar($name, $value);
    }

    private function fileContent($path, $dataToExtract=[]){
        extract($dataToExtract);
        ob_start();
        include($path);
        $content = ob_get_contents();
        ob_end_clean();
        return $this->replacements($content, $dataToExtract);
    }
    private function replacements($content){
        foreach ($this->data as $key=>$value){
            if(is_array($value)){
                foreach ($value as $_key=>$_value) {
                    if(is_array($_value)) continue;
                    $content = str_replace("{{" . $key . "." . $_key . "}}", $_value, $content);
                }
            }
            else $content = str_replace("{{".$key."}}", $value, $content);
        }
        return $content;
    }
    public function renderCss($path){
        $this->data["config"] = (array)Helper::config("app");
        if(!isset($this->data["config"]["resource_folder"])){
            echo "First set resource_folder in app/config/app";
            exit;
        }
        $file = "app/view/".$this->data["config"]["resource_folder"]."/css/".$path.".css";
        if(file_exists($file)) {
            if(strpos($path, '.min') != -1){
                echo $this->fileContent($file);
                exit;
            }
            $data = preg_replace(
                array(
                    // Remove comment(s)
                    '#\s*("(?:[^"\\\]++|\\\.)*+"|\'(?:[^\'\\\\]++|\\\.)*+\')\s*|\s*\/\*(?!\!|@cc_on)(?>[\s\S]*?\*\/)\s*|\s*(?<![\:\=])\/\/.*(?=[\n\r]|$)|^\s*|\s*$#',
                    // Remove the last semicolon
                    '#;+\}#',
                ),
                array(
                    '$1',
                    '}',
                ),
                $this->fileContent($file));
            $data = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $data);
            echo $data;
        }
    }
    public function renderJs($path){
        $this->data["config"] = (array)Helper::config("app");
        if(!isset($this->data["config"]["resource_folder"])){
            echo "First set resource_folder in app/config/app";
            exit;
        }
        $file = "app/view/".$this->data["config"]["resource_folder"]."/js/".$path.".js";
        if(file_exists($file)) {
            if(strpos($path, ".min") !== false) echo $this->fileContent($file);
            else echo preg_replace(
                array(
                    // Remove comment(s)
                    '#\s*("(?:[^"\\\]++|\\\.)*+"|\'(?:[^\'\\\\]++|\\\.)*+\')\s*|\s*\/\*(?!\!|@cc_on)(?>[\s\S]*?\*\/)\s*|\s*(?<![\:\=])\/\/.*(?=[\n\r]|$)|^\s*|\s*$#',
                    // Remove white-space(s) outside the string and regex
                    '#("(?:[^"\\\]++|\\\.)*+"|\'(?:[^\'\\\\]++|\\\.)*+\'|\/\*(?>.*?\*\/)|\/(?!\/)[^\n\r]*?\/(?=[\s.,;]|[gimuy]|$))|\s*([!%&*\(\)\-=+\[\]\{\}|;:,.<>?\/])\s*#s',
                    // Remove the last semicolon
                    '#;+\}#',
                    // Minify object attribute(s) except JSON attribute(s). From `{'foo':'bar'}` to `{foo:'bar'}`
                    '#([\{,])([\'])(\d+|[a-z_][a-z0-9_]*)\2(?=\:)#i',
                    // --ibid. From `foo['bar']` to `foo.bar`
                    '#([a-z0-9_\)\]])\[([\'"])([a-z_][a-z0-9_]*)\2\]#i'
                ),
                array(
                    '$1',
                    '$1$2',
                    '}',
                    '$1$3',
                    '$1.$3'
                ),
                $this->fileContent($file));
        }
    }

    public function rendered($path, $moreParams = []){
        if(!empty($moreParams))
            foreach($moreParams as $key=>$value) {
                $this->$key = $value;
            }
        if(isset($_SESSION))
            $this->data["session"] = $_SESSION;
        $this->data["config"] = (array)Helper::config("app");
        try {
            $loader = new Twig_Loader_Filesystem("app/view/");
            $twig = new Twig_Environment($loader);
            $twig->getExtension('Twig_Extension_Core');
            if(class_exists("\\app\\view\\TwigExtensions"))
                $twig->addExtension(new \app\view\TwigExtensions());
            return $twig->render($path.".twig", $this->data);
        }catch (\Exception $e){
            return $e->getMessage();
        }
    }

    public function render($path, $moreParams = [])
    {
        echo self::rendered($path, $moreParams);
    }
}
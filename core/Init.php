<?php
/**
 * Created by d0Nt
 * Date: 2018.05.30
 * Time: 12:11
 */

namespace core;


use core\Routing\Route;

class Init
{
    /**
     * Init all core functionality
     */
    public static function init()
    {
        $config = Helper::config("app");
        if(isset($config->dev_mode) && $config->dev_mode == true){
            error_reporting(E_ALL);
            ini_set('display_errors', 1);
        }else{
            error_reporting(E_ERROR);
            ini_set('display_errors', 0);
        }
        Session::start();
        require "Routing/DefaultRoutes.php";
        Routing\Offline::checkStatus();
        require "app/routing/Routes.php";
        if(!Route::isLoaded())
            Route::defaultRoute();
    }
    /**
     * Change default time zone to specified
     */
    public static function timeZone($zone){
        date_default_timezone_set ($zone);
    }
}
<?php
/**
 * Created by d0Nt
 * Date: 2018.05.11
 * Time: 21:54
 */

namespace core\Scheduler;


class CronRule
{
    /**
     * minute
     * hour
     * day of the mount
     * month of the year
     * @var string
     */
    private $cronLine = "* * * *";

    /**
     * Next run timestamp
     * @return false|int
     */
    public function nextRun(){
        $stamp = time();
        $stamp = $this->minute($stamp);
        $stamp = $this->hour($stamp);
        $stamp = $this->day($stamp);
        $stamp = $this->month($stamp);
        return $stamp;
    }
    private function minute($stamp){
        $cron = explode(' ', $this->cronLine);
        $minute = $cron[0];
        if($minute == "*"){
            if($stamp>time())
                return $stamp;
            $stamp = strtotime("+1 minute", $stamp);
        }
        else if(date("i", $stamp) >= $minute){
            $stamp = strtotime(($minute-date("i", $stamp))." minutes", $stamp);
            $stamp = strtotime("+1 hour", $stamp);
        }
        else if(date("i", $stamp) < $minute){
            $stamp = strtotime(($minute-date("i", $stamp))." minutes", $stamp);
        }
        return $stamp;
    }
    private function hour($stamp){
        $cron = explode(' ', $this->cronLine);
        $hour = $cron[1];
        if($hour == "*"){
            return $stamp;
        }
        else if(date("H", $stamp) >= $hour){
            $stamp = strtotime(($hour-date("H", $stamp))." hours", $stamp);
            $stamp = strtotime("+1 day", $stamp);
            $stamp = strtotime("-".date("i", $stamp)." minutes", $stamp);
        }
        else if(date("H", $stamp) < $hour){
            $stamp = strtotime(($hour-date("H", $stamp))." hours", $stamp);
            $stamp = strtotime("-".date("i", $stamp)." minutes", $stamp);
        }
        $stamp = $this->minute($stamp);
        return $stamp;
    }
    private function day($stamp){
        $cron = explode(' ', $this->cronLine);
        $day = $cron[2];
        if($day == "*"){
            return $stamp;
        }else if(date("d", $stamp) >= $day) {
            $stamp = strtotime(($day-date("d", $stamp))." days", $stamp);
            $stamp = strtotime("+1 month", $stamp);
            $stamp = strtotime("-".date("H", $stamp)." hours", $stamp);
            $stamp = strtotime("-".date("i", $stamp)." minutes", $stamp);
        }
        else if(date("d", $stamp) < $day){
            $stamp = strtotime(($day-date("d", $stamp))." days", $stamp);
            $stamp = strtotime("-".date("H", $stamp)." hours", $stamp);
            $stamp = strtotime("-".date("i", $stamp)." minutes", $stamp);
        }
        $stamp = $this->hour($stamp);
        return $stamp;
    }
    private function month($stamp){
        $cron = explode(' ', $this->cronLine);
        $month = $cron[3];
        if($month == "*"){
            return $stamp;
        }else if(date("m", $stamp) >= $month) {
            $stamp = strtotime(($month-date("m", $stamp))." months", $stamp);
            $stamp = strtotime("+1 year", $stamp);
            $stamp = strtotime("-".date("d", $stamp)." days", $stamp);
            $stamp = strtotime("-".date("H", $stamp)." hours", $stamp);
            $stamp = strtotime("-".date("i", $stamp)." minutes", $stamp);
        }
        else if(date("m", $stamp) < $month){
            $stamp = strtotime(($month-date("m", $stamp))." months", $stamp);
            $stamp = strtotime("-".date("d", $stamp)." days", $stamp);
            $stamp = strtotime("-".date("H", $stamp)." hours", $stamp);
            $stamp = strtotime("-".date("i", $stamp)." minutes", $stamp);
        }
        $stamp = $this->day($stamp);
        return $stamp;
    }

    /**
     * Run job every day at specified hour(0 default)
     * @param int $hour
     * @return $this
     */
    public function daily($hour = 0){
        if($hour>23 || $hour<0) $hour = 0;
        $this->cronLine = "* $hour * *";
        return $this;
    }

    /**
     * Run job every hour at specified minute(0 default)
     * @param int $minute
     * @return $this
     */
    public function hourly($minute = 0){
        if($minute>59 || $minute<0) $minute = 0;
        $this->cronLine = "$minute * * *";
        return $this;
    }

    /**
     * Run job every month at specified day(1 default)
     * @param int $day
     * @return $this
     */
    public function monthly($day = 1){
        if($day>28 || $day<1) $day = 1;
        $this->cronLine = "* * $day *";
        return $this;
    }

    /**
     * Run job every year at specified month(1 default)
     * @param int $month
     * @return $this
     */
    public function yearly($month = 1){
        if($month>12 || $month<1) $month = 1;
        $this->cronLine = "* * * $month";
        return $this;
    }
}
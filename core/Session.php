<?php
/**
 * Created by d0Nt
 * Date: 2017.03.23
 * Time: 21:35
 */

namespace core;


class Session
{
    public static function start(){
        $session_length = isset(Helper::config("app")->session_length)?Helper::config("app")->session_length:600;
        if (isset($_COOKIE['PHPSESSID']) && !preg_match('/^[a-z0-9]+$/', $_COOKIE['PHPSESSID'])) {
            unset($_COOKIE['PHPSESSID']);
            session_start();
            session_regenerate_id();
            return true;
        }
        $reporting = error_reporting();
        error_reporting(null);
        if(!session_start()){
            unset($_COOKIE['PHPSESSID']);
            session_start();
            session_regenerate_id();
        }
        if(Session::get("lastActive") !== false && Session::get("lastActive") <= time() - $session_length){
            Session::destroy();
            session_regenerate_id();
            return false;
        }
        Session::set("lastActive", time());
        error_reporting($reporting);
        return true;
    }

    public static function isLogged(){
        if((Session::get("lastActive")+600) < time())
            return false;
        if(Session::get("id") == false || Session::get("id") < 1)
            return false;
        return true;
    }

    public static function get($key){
        return isset($_SESSION[$key])? $_SESSION[$key] : false;
    }
    public static function set($key, $value){
        $_SESSION[$key] = $value;
    }
    public static function resetId(){
        session_regenerate_id(true);
    }
    public static function destroy(){
        session_destroy();
    }
}
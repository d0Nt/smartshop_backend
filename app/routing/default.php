<?php
/**
 * Cronjob route
 */
\core\Route::get("system/cronjobs/run", function(){
    if(!file_exists('app/schedules/')) die("app/schedules/ folder not exist");
    foreach (glob('app/schedules/*.php') as $file)
    {
        $class = str_replace(".php", "", $file);
        $class = str_replace("/", "\\", $class);
        if (class_exists($class)) {
            $job = new $class;
            if(is_subclass_of($job, 'core\\Scheduler\\Job'))
                $job->run();
        }
    }
});
/**
 * Returns image from view directory
 */
\core\Route::get("images/{image}", function($image){
    $file = "app/view/images/".$image;
    if(!file_exists($file)) return;
    $fp = fopen($file, 'rb');
    if($fp === false) return;
    header("Content-Type: image/png");
    header("Content-Length: " . filesize($file));
    fpassthru($fp);
    exit;
});
/**
 * Returns style from view directory
 */
\core\Route::get("styles/{style}", function($style){
    header('Content-Type: text/css');
    $file ="app/view/css/".$style.".css";

    if(!file_exists($file))
        echo "Failed to load style file $style.css";
    else
        (new \core\View())->renderCss($style);
    exit;
});
/**
 * Returns javascript from view directory
 */
\core\Route::get("scripts/{js}", function($js){
    header('Content-Type: application/javascript');
    $file ="app/view/js/".$js.".js";

    if(!file_exists($file))
        echo "Failed to load javascript file $js.js file";
    else
        (new \core\View())->renderJs($js);
    exit;
});
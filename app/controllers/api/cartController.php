<?php
namespace app\controllers\api;
use app\models\Cart;
use app\models\CartItem;
use app\models\ErrorCode;
use app\models\Item;
use app\models\User;
use app\system\communication;
use core\Controller;
use core\Redirection;

class cartController extends Controller
{
    public function index(){
        Redirection::home();
    }
    public function create(){
        $data = communication::processInput();
        if(!isset($data->user_id) || !is_numeric($data->user_id)){
            communication::processOutput(["error" => "Blogai pateiktas pirkėjo id.", "code" => ErrorCode::BAD_USER_ID]);
            return;
        }
        $user = new User($data->user_id);
        if(!$user->isInDatabase()){
            communication::processOutput(["error" => "Blogai pateiktas pirkėjo id.", "code" => ErrorCode::BAD_USER_ID]);
            return;
        }
        if($user->token != $data->token){
            communication::processOutput(["error" => "Prieiga nesuteikta.", "code" => ErrorCode::NO_ACCESS]);
            return;
        }
        $id = Cart::createOrGetLast($user->id);
        if($id == null){
            communication::processOutput(["error" => "Nepavyko sukurti krepšelio.", "code" => ErrorCode::OPERATION_FAILED]);
            return;
        }
        communication::processOutput(["cart_id" => $id]);
    }
    public function getActive(){
        $data = communication::processInput();
        if(!isset($data->user_id) || !is_numeric($data->user_id)){
            communication::processOutput(["error" => "Blogai pateiktas pirkėjo id.", "code" => ErrorCode::BAD_USER_ID]);
            return;
        }
        $user = new User($data->user_id);
        if(!$user->isInDatabase()){
            communication::processOutput(["error" => "Blogai pateiktas pirkėjo id.", "code" => ErrorCode::BAD_USER_ID]);
            return;
        }
        if($user->token != $data->token){
            communication::processOutput(["error" => "Prieiga nesuteikta.", "code" => ErrorCode::NO_ACCESS]);
            return;
        }
        $id = Cart::getActiveCart($user->id);
        if($id == null){
            communication::processOutput(["cart_id" => 0]);
            return;
        }
        communication::processOutput(["cart_id" => $id]);
    }

    public function pay(){
        $data = communication::processInput();
        if(!isset($data->user_id) || !is_numeric($data->user_id)){
            communication::processOutput(["error" => "Blogai pateiktas pirkėjo id.", "code" => ErrorCode::BAD_USER_ID]);
            return;
        }
        $user = new User($data->user_id);
        if(!$user->isInDatabase()){
            communication::processOutput(["error" => "Blogai pateiktas pirkėjo id.", "code" => ErrorCode::BAD_USER_ID]);
            return;
        }
        if($user->token != $data->token){
            communication::processOutput(["error" => "Prieiga nesuteikta.", "code" => ErrorCode::NO_ACCESS]);
            return;
        }
        if(!isset($data->cart_id) || !is_numeric($data->cart_id)){
            communication::processOutput(["error" => "Blogai pateiktas krepšelio id.", "code" => ErrorCode::BAD_CART_ID]);
            return;
        }
        $cart = new Cart($data->cart_id);
        if($cart == null || !$cart->isInDatabase()){
            communication::processOutput(["error" => "Blogai pateiktas krepšelio id.", "code" => ErrorCode::BAD_CART_ID]);
            return;
        }
        if($cart->pay_date != null){
            communication::processOutput(["error" => "Šio krepšelio prekės jau apmokėtos", "code" => ErrorCode::CART_PAID]);
            return;
        }
        $items = $cart->getCartItems();
        if(empty($items)){
            $cart->delete();
            communication::processOutput(["ok" => "ok"]);
            return;
        }
        $cart->price = 0.0;
        foreach ($items as $cartItem) {
            $itemData = new Item($cartItem->item_code);
            $cart->price += $itemData->price_per_item*$cartItem->amount;
        }
        /*
         * TODO: Payment logic
         */
        $cart->pay_date = date('Y-m-d H:i:s');
        $cart->save();
        communication::processOutput(["ok" => "ok"]);
    }

    public function addItem(){
        $data = communication::processInput();
        if(!isset($data->user_id) || !is_numeric($data->user_id)){
            communication::processOutput(["error" => "Blogai pateiktas pirkėjo id.", "code" => ErrorCode::BAD_USER_ID]);
            return;
        }
        $user = new User($data->user_id);
        if(!$user->isInDatabase()){
            communication::processOutput(["error" => "Blogai pateiktas pirkėjo id.", "code" => ErrorCode::BAD_USER_ID]);
            return;
        }
        if($user->token != $data->token){
            communication::processOutput(["error" => "Prieiga nesuteikta.", "code" => ErrorCode::NO_ACCESS]);
            return;
        }
        if(!isset($data->cart_id) || !is_numeric($data->cart_id)){
            communication::processOutput(["error" => "Blogai pateiktas krepšelio id.", "code" => ErrorCode::BAD_CART_ID]);
            return;
        }
        $cart = new Cart($data->cart_id);
        if($cart == null || !$cart->isInDatabase()){
            communication::processOutput(["error" => "Blogai pateiktas krepšelio id.", "code" => ErrorCode::BAD_CART_ID]);
            return;
        }
        if(!isset($data->item_code)){
            communication::processOutput(["error" => "Nenurodytas prekės kodas.", "code" => ErrorCode::NO_ITEM]);
            return;
        }
        $item = new Item($data->item_code);
        if($item == null || !$item->isInDatabase()){
            communication::processOutput(["error" => "Nenurodyta prekė neegzistuoja duomenų bazėje.", "code" => ErrorCode::BAD_ITEM_CODE]);
            return;
        }
        CartItem::addItem($cart->cart_id, $item->item_code);
        $this->itemsList();
    }

    public function removeItem(){
        $data = communication::processInput();
        if(!isset($data->user_id) || !is_numeric($data->user_id)){
            communication::processOutput(["error" => "Blogai pateiktas pirkėjo id.", "code" => ErrorCode::BAD_USER_ID]);
            return;
        }
        $user = new User($data->user_id);
        if(!$user->isInDatabase()){
            communication::processOutput(["error" => "Blogai pateiktas pirkėjo id.", "code" => ErrorCode::BAD_USER_ID]);
            return;
        }
        if($user->token != $data->token){
            communication::processOutput(["error" => "Prieiga nesuteikta.", "code" => ErrorCode::NO_ACCESS]);
            return;
        }
        if(!isset($data->cart_id) || !is_numeric($data->cart_id)){
            communication::processOutput(["error" => "Blogai pateiktas krepšelio id.", "code" => ErrorCode::BAD_CART_ID]);
            return;
        }
        $cart = new Cart($data->cart_id);
        if($cart == null || !$cart->isInDatabase()){
            communication::processOutput(["error" => "Blogai pateiktas krepšelio id.", "code" => ErrorCode::BAD_CART_ID]);
            return;
        }
        if(!isset($data->item_id)){
            communication::processOutput(["error" => "Nenurodytas prekės id.", "code" => ErrorCode::BAD_ITEM_CODE]);
            return;
        }
        $item = new CartItem($data->item_id);
        if($item == null || !$item->isInDatabase()){
            communication::processOutput(["error" => "Nenurodytas pirkinys neegzistuoja duomenų bazėje.", "code" => ErrorCode::NO_ITEM]);
            return;
        }
        $item->remove();
        $this->itemsList();
    }
    public function itemsList(){
        $data = communication::processInput();
        if(!isset($data->user_id) || !is_numeric($data->user_id)){
            communication::processOutput(["error" => "Blogai pateiktas pirkėjo id.", "code" => ErrorCode::BAD_USER_ID]);
            return;
        }
        $user = new User($data->user_id);
        if(!$user->isInDatabase()){
            communication::processOutput(["error" => "Blogai pateiktas pirkėjo id.", "code" => ErrorCode::BAD_USER_ID]);
            return;
        }
        if($user->token != $data->token){
            communication::processOutput(["error" => "Prieiga nesuteikta.", "code" => ErrorCode::NO_ACCESS]);
            return;
        }
        if(!isset($data->cart_id) || !is_numeric($data->cart_id)){
            communication::processOutput(["error" => "Prieiga nesuteikta.", "code" => ErrorCode::NO_ACCESS]);
            return;
        }
        $cart = new Cart($data->cart_id);
        if($cart == null || !$cart->isInDatabase()){
            communication::processOutput(["error" => "Blogai pateiktas krepšelio id.", "code" => ErrorCode::BAD_CART_ID]);
            return;
        }
        $array = [];
        foreach ($cart->getCartItems() as $cartItem) {
            $item = [];
            $item["id"] = $cartItem->id;
            $item["amount"] = $cartItem->amount;
            $itemData = new Item($cartItem->item_code);
            $item["name"] = $itemData->name;
            $item["price"] = $itemData->price_per_item;
            array_push($array, $item);
        }
        communication::processOutput($array);
    }

    public function history(){
        $data = communication::processInput();
        if(!isset($data->user_id) || !is_numeric($data->user_id)){
            communication::processOutput(["error" => "Blogai pateiktas pirkėjo id.", "code" => ErrorCode::BAD_USER_ID]);
            return;
        }
        $user = new User($data->user_id);
        if(!$user->isInDatabase()){
            communication::processOutput(["error" => "Blogai pateiktas pirkėjo id.", "code" => ErrorCode::BAD_USER_ID]);
            return;
        }
        if($user->token != $data->token){
            communication::processOutput(["error" => "Prieiga nesuteikta.", "code" => ErrorCode::NO_ACCESS]);
            return;
        }
        $data = Cart::getCartsHistory($user->id);
        $response = [];
        foreach ($data as $cart){
            $cart = (object)$cart;
            $cartData = [];
            $cartData["id"] = $cart->cart_id;
            $cartData["created"] = $cart->create_date;
            $cartData["paid"] = $cart->pay_date;
            $cartData["price"] = $cart->price;
            $cartData["items"] = [];
            foreach ($cart->getCartItems() as $item){
                $itemData = [];
                $itemData["id"] = $item->id;
                $itemData["amount"] = $item->amount;
                $itemInfo = new Item($item->item_code);
                $itemData["name"] = $itemInfo->name;
                $itemData["price"] = $itemInfo->price_per_item;
                array_push($cartData["items"], $itemData);
            }
            array_push($response, $cartData);
        }
        communication::processOutput($response);
    }
}
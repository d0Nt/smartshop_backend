<?php
namespace app\controllers\api;
use app\models\ErrorCode;
use app\models\User;
use app\system\communication;
use core\Controller;
use core\Database\Field;
use core\Redirection;

class userController extends Controller
{
    public function index()
    {
        Redirection::home();
    }

    private function validateData($data, $require = false){
        if(!isset($data->email) && $require || isset($data->email) && (strlen($data->email) < 2 || !filter_var($data->email, FILTER_VALIDATE_EMAIL))){
            communication::processOutput(["error" => "Neįvestas arba neteisingas el. pašto adresas.", "error_field" => "email", "code" => ErrorCode::BAD_INPUT]);
            return false;
        }
        if(!isset($data->firstName) && $require || isset($data->firstName) && strlen($data->firstName) < 2){
            communication::processOutput(["error" => "Neįvestas vardas.", "error_field" => "firstName", "code" => ErrorCode::BAD_INPUT]);
            return false;
        }
        if(!isset($data->lastName) && $require || isset($data->lastName) && strlen($data->lastName) < 2){
            communication::processOutput(["error" => "Neįvesta pavardė.", "error_field" => "lastName", "code" => ErrorCode::BAD_INPUT]);
            return false;
        }
        return true;
    }

    public function requestResetPassword(){
        $data = communication::processInput(false);
        if(!isset($data->email)){
            communication::processOutput(["error" => "Neįvestas el. pašto adresas", "error_field" => "email", "code" => ErrorCode::BAD_INPUT]);
            return;
        }
        if(strlen($data->email) < 2 || !filter_var($data->email, FILTER_VALIDATE_EMAIL)){
            communication::processOutput(["error" => "Blogai įvestas el. pašto adresas.", "error_field" => "email", "code" => ErrorCode::BAD_INPUT]);
            return;
        }
        $user = User::getByFields([
            new Field("email", $data->email)
        ]);
        if($user == null){
            communication::processOutput(["error" => "Toks vartotojas neegzistuoja.", "error_field" => "email", "code" => ErrorCode::BAD_INPUT]);
            return;
        }
        communication::processOutput(["ok" => "ok"]);
    }
    public function register(){
        $data = communication::processInput(false);
        if(!$this->validateData($data, true)) return;
        if(!isset($data->password) || strlen($data->password) < 2){
            communication::processOutput(["error" => "Neįvestas slaptažodis.", "error_field" => "password", "code" => ErrorCode::BAD_INPUT]);
            return;
        }
        $user = User::getByFields([
            new Field("email", $data->email)
        ]);
        if($user != null)
        {
            communication::processOutput(["error" => "El. pašto adresas jau naudojamas.", "error_field" => "email", "code" => ErrorCode::BAD_INPUT]);
            return;
        }
        $user = new User();
        $user->email = $data->email;
        $user->first_name = ucfirst(strtolower($data->firstName));
        $user->last_name = ucfirst(strtolower($data->lastName));
        $user->password = strtoupper(hash("sha256", $data->password));
        $user->insert();
        communication::processOutput(["success" => "Vartotojas užregistruotas."]);
    }
    public function delete(){
        $data = communication::processInput();
        if(!isset($data->user_id) || !is_numeric($data->user_id)){
            communication::processOutput(["error" => "Blogai pateiktas pirkėjo id.", "code" => ErrorCode::BAD_USER_ID]);
            return;
        }
        $user = new User($data->user_id);
        if(!$user->isInDatabase()){
            communication::processOutput(["error" => "Blogai pateiktas pirkėjo id.", "code" => ErrorCode::BAD_USER_ID]);
            return;
        }
        if($user->token != $data->token){
            communication::processOutput(["error" => "Prieiga nesuteikta.", "code" => ErrorCode::NO_ACCESS]);
            return;
        }
        $user->delete_request = date('Y-m-d H:i:s', strtotime('+30 days', time()));
        $user->save();
        communication::processOutput(["deleted" => true]);
    }
    public function check(){
        $data = communication::processInput();
        if(!isset($data->token)){
            communication::processOutput(["error"=> "Prieiga nesuteikta.", "code" => ErrorCode::NO_ACCESS]);
            return;
        }
        if(!isset($data->user_id) || !is_numeric($data->user_id)){
            communication::processOutput(["error" => "Blogas pirkėjo id.", "code" => ErrorCode::BAD_USER_ID]);
            return;
        }
        $user = new User($data->user_id);
        if(!$user->isInDatabase()){
            communication::processOutput(["error" => "Blogas vartotojo id.", "code" => ErrorCode::BAD_USER_ID]);
            return;
        }
        if($user->token != $data->token){
            communication::processOutput(["error" => "Klaidinga sesija.", "code" => ErrorCode::NO_ACCESS]);
            return;
        }
        communication::processOutput(["id" => $user->id, "token"=>$user->token]);

    }
    public function getData(){
        $data = communication::processInput();
        if(!isset($data->token)){
            communication::processOutput(["error"=> "Prieiga nesuteikta.", "code" => ErrorCode::NO_ACCESS]);
            return;
        }
        if(!isset($data->user_id) || !is_numeric($data->user_id)){
            communication::processOutput(["error" => "Blogas pirkėjo id.", "code" => ErrorCode::BAD_USER_ID]);
            return;
        }
        $user = new User($data->user_id);
        if(!$user->isInDatabase()){
            communication::processOutput(["error" => "Blogas vartotojo id.", "code" => ErrorCode::BAD_USER_ID]);
            return;
        }
        if($user->token != $data->token){
            communication::processOutput(["error" => "Klaidinga sesija.", "code" => ErrorCode::NO_ACCESS]);
            return;
        }
        communication::processOutput(["email"=>$user->email, "firstName"=>$user->first_name, "lastName"=>$user->last_name]);
    }
    public function changePassword(){
        $data = communication::processInput();
        if(!isset($data->token)){
            communication::processOutput(["error"=> "Prieiga nesuteikta.", "code" => ErrorCode::NO_ACCESS]);
            return;
        }
        if(!isset($data->user_id) || !is_numeric($data->user_id)){
            communication::processOutput(["error" => "Blogas pirkėjo id.", "code" => ErrorCode::BAD_USER_ID]);
            return;
        }
        $user = new User($data->user_id);
        if(!$user->isInDatabase()){
            communication::processOutput(["error" => "Blogas vartotojo id.", "code" => ErrorCode::BAD_USER_ID]);
            return;
        }
        if($user->token != $data->token){
            communication::processOutput(["error" => "Klaidinga sesija.", "code" => ErrorCode::NO_ACCESS]);
            return;
        }
        if(!isset($data->old) || $user->password != strtoupper(hash("sha256", $data->old))){
            communication::processOutput(["error" => "Senas slaptažodis neteisingas.", "error_field" => "old", "code" => ErrorCode::BAD_INPUT]);
            return;
        }
        if(!isset($data->new)){
            communication::processOutput(["error" => "Naujas slaptažodis neįvestas.", "error_field" => "new", "code" => ErrorCode::BAD_INPUT]);
            return;
        }
        $user->password = strtoupper(hash("sha256", $data->new));
        $user->save();
        communication::processOutput(["ok" => "ok"]);
    }
    public function saveData(){
        $data = communication::processInput();
        if(!isset($data->token)){
            communication::processOutput(["error"=> "Prieiga nesuteikta.", "code" => ErrorCode::NO_ACCESS]);
            return;
        }
        if(!isset($data->user_id) || !is_numeric($data->user_id)){
            communication::processOutput(["error" => "Blogas pirkėjo id.", "code" => ErrorCode::BAD_USER_ID]);
            return;
        }
        $user = new User($data->user_id);
        if(!$user->isInDatabase()){
            communication::processOutput(["error" => "Blogas vartotojo id.", "code" => ErrorCode::BAD_USER_ID]);
            return;
        }
        if($user->token != $data->token){
            communication::processOutput(["error" => "Klaidinga sesija.", "code" => ErrorCode::NO_ACCESS]);
            return;
        }
        if(!$this->validateData($data)) return;
        if(isset($data->email)) $user->email = $data->email;
        if(isset($data->firstName)) $user->first_name = ucfirst(strtolower($data->firstName));
        if(isset($data->lastName)) $user->last_name = ucfirst(strtolower($data->lastName));
        $user->save();
        communication::processOutput(["email"=>$user->email, "firstName"=>$user->first_name, "lastName"=>$user->last_name]);
    }
    public function login(){
        $data = communication::processInput(false);
        if(!isset($data->email) || strlen($data->email) < 2 || !filter_var($data->email, FILTER_VALIDATE_EMAIL)){
            communication::processOutput(["error" => "Neįvestas arba neteisingas el. pašto adresas.", "error_field" => "email", "code" => ErrorCode::BAD_INPUT]);
            return;
        }
        if(!isset($data->password) || strlen($data->password) < 2){
            communication::processOutput(["error" => "Neįvestas slaptažodis.", "error_field" => "password", "code" => ErrorCode::BAD_INPUT]);
            return;
        }

        $user = User::getByFields([
            new Field("email", $data->email)
        ]);
        if($user == null || !$user->isInDatabase())
        {
            communication::processOutput(["error" => "Vartotojo el. paštas arba slaptažodis neteisingas.", "error_field" => "email", "code" => ErrorCode::BAD_INPUT]);
        }
        else if($data->email == $user->email && $user->password == strtoupper(hash("sha256", $data->password))) {
            $token = communication::token();
            $user->token = $token;
            $user->delete_request = null;
            $user->save();
            communication::processOutput(["id" => $user->id, "token" => $token]);
        }else
            communication::processOutput(["error" => "Vartotojo vardas arba slaptažodis neteisingas.", "error_field" => "email", "code" => ErrorCode::BAD_INPUT]);
    }
}
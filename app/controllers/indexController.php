<?php
namespace app\controllers;
use app\schedules\ExampleJob;
use core\Controller;
use core\Redirection;
use core\View;

class indexController extends Controller
{
    public function index()
    {
        $view = new View();
        $view->title = "phpFramework";
        $view->render("index");
    }
}
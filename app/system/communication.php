<?php
/**
 * Created by d0Nt
 * Date: 2019.01.14
 * Time: 14:47
 */

namespace app\system;


class communication
{
    private static function readToken(){
        $token = null;
        $headers = apache_request_headers();
        if(isset($headers['authorization'])){
            $matches = array();
            preg_match('/Token token="(.*)"/', $headers['authorization'], $matches);
            if(isset($matches[1])){
                $token = $matches[1];
            }
        }
        return $token;
    }
    public static function processInput($requireToken = true){
        $token = null;
        if($requireToken){
            $token = self::readToken();
            if($token == null){
                self::processOutput(["error" => "Prieiga negalima."]);
                die();
            }
        }
        $array = [];
        if(isset($_SERVER["CONTENT_TYPE"]) && strpos($_SERVER["CONTENT_TYPE"], 'application/json') !== false){
            try {
                $array = (array)json_decode(file_get_contents('php://input'));
            }catch (\Exception $e){
                self::processOutput(["error" => "Blogai pateikti užklausos parametrai"]);
                die();
            }
        }
        $array["size"] = count($array);
        $array["token"] = $token;
        return (object)$array;
    }
    public static function processOutput($array){
        echo json_encode($array);
    }

    public static function token(){
        try {
            return bin2hex(random_bytes(32));
        } catch (\Exception $e) {
            return substr(base64_encode(communication::randomString(64)),0,64);
        }
    }
    public static function randomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
<?php
return [
    'dev_mode' => true,
    'directory' => '/SmartShop',
    'resource_folder' => 'app_resources',
    'title' => "SmartShop"
];
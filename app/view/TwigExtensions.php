<?php
/**
 * Created by d0Nt
 * Date: 2018.07.29
 * Time: 21:24
 */

namespace app\view;


use app\models\admin\User;
use core\Session;
use Twig_Extension;
use Twig_Extension_GlobalsInterface;

class TwigExtensions extends Twig_Extension implements Twig_Extension_GlobalsInterface
{
    public function getFilters()
    {
        return [];
    }

    public function getGlobals()
    {
        if(Session::isLogged()){
            $globals["me"] = User::me()->getArray();

        }
        $globals["me"]["logged"] = Session::isLogged();
        return $globals;

    }
}
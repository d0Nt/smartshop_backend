function printElement(id, font_size=50) {
    let clone = $(id).clone();
    clone.find(".no-print").remove();
    clone.css({'font-size':font_size+'px'});
    clone.printThis();
}
$(() => {
    function loadPageData(url, params = [], type="json"){
        return new Promise(resolve => {
            $.post(url, params, (data) => {
                resolve(data);
            }, type);
        });
    }
    $(".toggle-nav").click(()=>{
        $(".sidebar").addClass("enabledMobile");
    });
    $(".hide-nav").click(()=>{
        $(".sidebar").removeClass("enabledMobile");
    });

    $("#pagination").on('click', '.page-item', function(element){
        if($(element.target).hasClass("disabled") === true) return;
        updateTableData($(".table"), $(element.target).data("page"), {name:$("#search").data("filter-name"),data:$("#search").val()});
    });
    function updatePagination(data, url){
        let content = "";
        let pagination = $("#pagination");
        pagination.data("url", url);
        if(data.page === 1)
            content = "<li class=\"page-item disabled\"><a class=\"page-link\" data-page=\"-1\">Ankstesnis</a></li>";
        else
            content = "<li class=\"page-item\"><a class=\"page-link\" data-page=\""+(data.page-1)+"\">Ankstesnis</a></li>";
        if(data.numberOfPages < 6){
            for(let i=1; i<=data.numberOfPages; i++) {
                if(i === data.page)
                    content += '<li class="page-item disabled"><a class="page-link" data-page="' + i + '">' + i + '</a></li>';
                else
                    content += '<li class="page-item"><a class="page-link" data-page="' + i + '">' + i + '</a></li>';
            }
        }else{
            for(let i=data.page-2; i<=data.page+1; i++) {
                if(i === data.page)
                    content += '<li class="page-item disabled"><a class="page-link" data-page="' + i + '">' + i + '</a></li>';
                else
                    content += '<li class="page-item"><a class="page-link" data-page="' + i + '">' + i + '</a></li>';
            }
        }
        if(data.page === data.numberOfPages)
            content += "<li class=\"page-item disabled\"><a class=\"page-link\" data-page=\"+1\">Sekantis</a></li>";
        else
            content += "<li class=\"page-item\"><a class=\"page-link\" data-page=\""+(data.page+1)+"\">Sekantis</a></li>";
        pagination.html(content);
    }
    $("#search").change(function () {
        updateTableData($(".table"), 1, {name:$("#search").data("filter-name"),data:$("#search").val()});
    });
    function updateTableData(element, page, filter){
        loadPageData($(element).data("url"), {page: page, filter_name: filter.name, filter: filter.data})
            .then((data) => {
                let table_data = "";
                if(data.data.length < 1)
                    table_data = "Duomenų nėra.";
                else data.data.map(item => {
                    let row = "<tr>";
                    for (let key in item) {
                        if (item.hasOwnProperty(key)) {
                            if(typeof $(element).data("exclude") !== 'undefined' && jQuery.inArray(key, $(element).data("exclude")) !== -1)
                                continue;
                            row+="<td>"+item[key]+"</td>";
                        }
                    }
                    let column = "<td class='no-print'>";
                    if(typeof $(element).data("edit-url") !== 'undefined')
                        column+="<a href='"+$(element).data("edit-url")+""+item[Object.keys(item)[0]]+"'><i class='fas fa-pencil-alt'></i></a> ";
                    if(typeof $(element).data("delete-url") !== 'undefined')
                        column+="<a href='"+$(element).data("delete-url")+""+item[Object.keys(item)[0]]+"'><i class='far fa-trash-alt'></i></a> ";

                    if(column !== "<td>")
                        row+=column+"</td>";
                    table_data+=row+"</tr>";
                });
                $(element).find("tbody").html(table_data);
                updatePagination(data, $(element).data("url"));
            }).catch((error)=>{
            alert(error);
        })
    }
    $(".table").each((index, element) => {
       if(typeof $(element).data("url") !== 'undefined'){
           updateTableData(element, 1, {name: "", data:""});
       }
    });
});
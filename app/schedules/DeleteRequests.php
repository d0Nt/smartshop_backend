<?php
/**
 * Created by d0Nt
 * Date: 2018.05.12
 * Time: 17:21
 */

namespace app\schedules;

use app\models\Cart;
use app\models\User;
use core\Database\Field;
use core\Scheduler\Job;

class DeleteRequests extends Job
{
    protected function scheduler()
    {
        $this->schedule()->daily(0);
    }

    protected function command()
    {
        $users = User::getByFields(Field::customSeparator("delete_request", date("Y-m-d H:i:s"), "<"));
        if(is_null($users)) return;
        if(is_array($users)){
            foreach ($users as $user){
                $this->removeUserCarts($user->id);
                $user->delete();
            }
        }else{
            $this->removeUserCarts($users->id);
            $users->delete();
        }
    }

    private function removeUserCarts($u_id){
        $carts = Cart::getByFields(new Field("user_id", $u_id));
        if($carts != null){
            if(is_array($carts)){
                foreach ($carts as $cart){
                    $cart->user_id = null;
                    $cart->save();
                }
            }else{
                $carts->user_id = null;
                $carts->save();
            }
        }
    }
}
<?php
/**
 * Created by d0Nt
 * Date: 2018.03.24
 * Time: 23:20
 */

namespace app\models;


use core\Database\Field;
use core\Model;

class User extends Model
{
    protected static $table = "user";
    protected static $idColumn = "id";
    protected static $selectFields = ["id", "first_name", "last_name", "email", "register_date", "password", "token", "delete_request"];
    protected static $saveFields = [
        "first_name", "last_name", "email", "register_date", "password", "token", "delete_request"
    ];


    public static function getItems($page, $perPage = 10, $select = "*", $where = null, $sort = null, $include_null = false)
    {
        $data = parent::getItems($page, $perPage, $select, $where, $sort);

        foreach ($data["data"] as $key => $item){
            unset( $data["data"][$key]["password"]);
            unset( $data["data"][$key]["token"]);

        }
        return $data;
    }

    public function getOrders()
    {
        $items = Cart::getByFields([
            new Field("user_id", $this->id)
        ]);
        if($items == null)
            return [];
        if(!is_array($items))
            return [$items];
        return $items;
    }
}
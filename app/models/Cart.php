<?php
/**
 * Created by d0Nt
 * Date: 2018.03.24
 * Time: 23:20
 */

namespace app\models;


use core\Database\Field;
use core\Model;

class Cart extends Model
{
    protected static $table = "cart";
    protected static $idColumn = "cart_id";
    protected static $selectFields = ["cart_id", "user_id", "create_date", "pay_date", "price"];
    protected static $saveFields = [
        "user_id", "create_date", "pay_date", "price"
    ];

    public static function createOrGetLast($user_id){
        $id = self::getActiveCart($user_id);
        if($id != NULL)
            return $id;
        $cart = new Cart();
        $cart->user_id = $user_id;
        $id = $cart->insert();
        return $id;
    }
    public static function getActiveCart($user_id){
        $result = self::getByFields([
            new Field("user_id", $user_id),
            Field::customSeparator("pay_date", "NULL", " IS ")->unsafe()
        ], ["cart_id", "desc"], 1);
        if($result != NULL)
            return $result->cart_id;
        else
            return null;
    }
    public static function getCartsHistory($user_id){
        $result = self::getByFields([
            new Field("user_id", $user_id),
            Field::customSeparator("pay_date", "NULL", " IS NOT ")->unsafe()
        ],["cart_id", "desc"]);
        if($result == NULL) return [];
        if(is_array($result)) return $result;
        else return [$result];
    }
    public function getCartItems(){
        $items = CartItem::getByFields([
            new Field("cart_id", $this->cart_id)
        ]);
        if($items == null)
            return [];
        if(!is_array($items))
            return [$items];
        return $items;
    }
}
<?php
/**
 * Created by d0Nt
 * Date: 2019-05-10
 * Time: 18:06
 */

namespace app\models;


class ErrorCode
{
    const BAD_USER_ID = "BAD_USER_ID";
    const BAD_CART_ID = "BAD_CART_ID";
    const BAD_ITEM_CODE = "BAD_ITEM_CODE";
    const NO_ITEM = "NO_ITEM";
    const BAD_INPUT = "BAD_INPUT";
    const CART_PAID = "CART_PAID";
    const NO_ACCESS = "NO_ACCESS";
    const OPERATION_FAILED = "FAILED";
}
<?php
/**
 * Created by d0Nt
 * Date: 2018.03.24
 * Time: 23:20
 */

namespace app\models;


use core\Database\Field;
use core\Model;

class CartItem extends Model
{
    protected static $table = "cart_item";
    protected static $idColumn = "id";
    protected static $selectFields = ["id", "cart_id", "item_code", "amount"];
    protected static $saveFields = ["cart_id", "item_code", "amount"];

    public static function addItem($cart_id, $item_code)
    {
        $items = self::getByFields([
            new Field("cart_id", $cart_id),
            new Field("item_code", $item_code)
        ]);
        if($items == null){
            $item = new CartItem();
            $item->cart_id = $cart_id;
            $item->item_code = $item_code;
            $item->amount = 1;
            $item->insert();
        }else{
            if(is_array($items)){
                $items = $items[0];
            }
            $items->amount++;
            $items->save();
        }
    }

    public function remove()
    {
        $this->amount--;
        if($this->amount == 0)
            $this->delete();
        else
            $this->save();
    }
}
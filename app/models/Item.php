<?php
/**
 * Created by d0Nt
 * Date: 2018.03.24
 * Time: 23:20
 */

namespace app\models;


use core\Database\Field;
use core\Model;

class Item extends Model
{
    protected static $table = "item";
    protected static $idColumn = "item_code";
    protected static $selectFields = ["item_code", "name", "amount", "price_per_item"];
    protected static $saveFields = ["name", "amount", "price_per_item"];

    public static function getItems($page, $perPage = 10, $select = "*", $where = null, $sort = null, $include_null = false)
    {
        $data =  parent::getItems($page, $perPage, $select, $where, $sort, true);
        foreach ($data["data"] as $key => $item){
            $data["data"][$key]["price_per_item"] = number_format($data["data"][$key]["price_per_item"], 2);
        }
        return $data;
    }

    public function getOrders()
    {
        $items = CartItem::getByFields([
            new Field("item_code", $this->item_code)
        ]);
        if($items == null)
            return [];
        if(!is_array($items))
            return [$items];
        return $items;
    }
}
-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: 2019 m. Kov 20 d. 12:26
-- Server version: 5.7.22-log
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kursinis`
--

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `cart`
--

CREATE TABLE `cart` (
  `cart_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `create_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `pay_date` datetime DEFAULT NULL,
  `price` float DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

--
-- Sukurta duomenų kopija lentelei `cart`
--

INSERT INTO `cart` (`cart_id`, `user_id`, `create_date`, `pay_date`, `price`) VALUES
(1, 1, '2019-01-16 21:09:42', '2019-01-16 21:09:42', 4.44),
(3, 1, '2019-01-16 21:09:42', NULL, 0);

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `cart_item`
--

CREATE TABLE `cart_item` (
  `id` int(11) NOT NULL,
  `cart_id` int(11) NOT NULL,
  `item_code` varchar(24) COLLATE utf8_lithuanian_ci NOT NULL,
  `amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

--
-- Sukurta duomenų kopija lentelei `cart_item`
--

INSERT INTO `cart_item` (`id`, `cart_id`, `item_code`, `amount`) VALUES
(3, 3, '5000112620054', 5);

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `item`
--

CREATE TABLE `item` (
  `item_code` varchar(24) COLLATE utf8_lithuanian_ci NOT NULL,
  `name` varchar(256) COLLATE utf8_lithuanian_ci NOT NULL,
  `amount` int(11) NOT NULL,
  `price_per_item` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

--
-- Sukurta duomenų kopija lentelei `item`
--

INSERT INTO `item` (`item_code`, `name`, `amount`, `price_per_item`) VALUES
('47702661857', 'test2', 2, 3.5),
('4770266185736', 'Test sandelys', 9, 10),
('4770266185737', 'test7', 8, 7),
('4770266185738', 'test10', 10, 10),
('4770266185739', 'test9', 9, 9),
('477026618574', 'Test3', 5, 8),
('4770266185740', 'test8', 8, 8),
('4770266185741', 'test6', 6, 6),
('4770266185742', 'test5', 5, 6),
('4770266185743', 'Test4', 4, 4),
('4770266185746', 'Test1', 1, 2.5),
('4770266185747', 'Saulėgrąžos \"Yes\"', 55, 1.01),
('5000112620054', 'Gaivusis gėrimas \"Fanta\"', 300, 155.9);

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `first_name` varchar(64) COLLATE utf8_lithuanian_ci NOT NULL,
  `last_name` varchar(64) COLLATE utf8_lithuanian_ci NOT NULL,
  `email` varchar(256) COLLATE utf8_lithuanian_ci NOT NULL,
  `register_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `password` varchar(128) COLLATE utf8_lithuanian_ci NOT NULL,
  `token` varchar(64) COLLATE utf8_lithuanian_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

--
-- Sukurta duomenų kopija lentelei `user`
--

INSERT INTO `user` (`id`, `first_name`, `last_name`, `email`, `register_date`, `password`, `token`) VALUES
(1, 'Vardenis', 'Pavardenis', 'ez@ez.lt', '2019-01-16 21:09:24', 'DAC8C3D901EC554C26EA8990CF3115351C33756E011A1E443ADF757895DAEF8C', 'cUQwWmFWd083VlZKYWY1MHg3QmV4UUp6');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`cart_id`),
  ADD KEY `FK_KURIA` (`user_id`);

--
-- Indexes for table `cart_item`
--
ALTER TABLE `cart_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_pirkinys` (`cart_id`),
  ADD KEY `FK_pirkinys2` (`item_code`);

--
-- Indexes for table `item`
--
ALTER TABLE `item`
  ADD PRIMARY KEY (`item_code`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `cart_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `cart_item`
--
ALTER TABLE `cart_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Apribojimai eksportuotom lentelėm
--

--
-- Apribojimai lentelei `cart`
--
ALTER TABLE `cart`
  ADD CONSTRAINT `FK_KURIA` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Apribojimai lentelei `cart_item`
--
ALTER TABLE `cart_item`
  ADD CONSTRAINT `FK_pirkinys` FOREIGN KEY (`cart_id`) REFERENCES `cart` (`cart_id`),
  ADD CONSTRAINT `FK_pirkinys2` FOREIGN KEY (`item_code`) REFERENCES `item` (`item_code`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
